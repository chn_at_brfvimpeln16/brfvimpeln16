Förvaltning
===========

Fastighetsskötsel
-----------------

Driftia förvaltning. Felanmälan via <a href="http://www.driftia.se/felanmalan" target="_blank">Driftia hemsida</a>

Telefon måndag - fredag 08.00 - 16.00 08-744 44 33

Akut jour övrig tid 08- 744 09 50

Hissinformation
---------------
Felanmälan görs till S:t Eriks hiss 08-522 258 00. Om man
fastnar i hissen kan man använda larmtelefonen i hissen som är
kopplad
till SOS Alarm. De tar sedan kontakt med S:t Eriks hiss.

Grannsämja och störningar
-------------------------
Om man blir störd av höga ljud från
en granne ska man kontakta den som stör och göra den
uppmärksam
på problemet. Ibland kanske grannen inte
ens vet om att den stör. Forsätter störningarna
ändå, kan styrelsen gå in och diskutera saken med
de berörda.

Bredband / TV
-------------
<a href="bredband.html">Till Bredbandssidan</a>.

Kontraktsärenden
----------------
Vår fastighet förvaltas
administrativt av Princip Redovisning. De
har hand om bostadsrättsförsäljningar, pantförskrivningar
och kontraktsfrågor vid lägenhetsbyten och andrahandsuthyrningar.

Medlemmar kan logga in hos hos förvaltaren med hjälp av Bank-ID
för att se avier och annan information.
[Till Portalen](https://www.principredovisning.se)

```
 Princip Redovisning
 Valhallavägen 165
 115 53 Stockholm
 info@principredovisning.se

 Anne-Marie Stenlund 070-739 56 50
 Jeanette Essnert Wahlbäck 0730-39 99 09
```
Parkeringsplatser
-----------------
Vi har ett 50-tal parkeringsplatser både utomhus och i garage
som ligger mellan vårt hus och Klippgatan 20. Kontakta
styrelsen om du vill ställa dig i kö eller säga
upp en plats.

Skadedjur
---------
Om du fått någon form av ohyra skall du kontakta Anticimex
så fort som möjligt. Du kan kontrollera vad det är
för typ av kryp du hittat på [Anticimex webbsida](https://www.anticimex.se/).

![](bilder/forvaltning.gif)
