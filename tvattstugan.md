Våra tvättstugor
================

Tvättstugorna har en viktig funktion i vårt hus.  Vi
har två tvättstugor och två torkrum. Om du
passar dina tvättider och tar hänsyn till dina grannar
fungerar de bra.

Bokning
-------
Tider bokas på bokningstavlan i tvättstugekorridoren genom
att låsa fast tvättcylindern på den lediga tid du vill boka.
Tvättcylinder beställes genom styrelsen. Till torkrummen
får du använda eget hänglås.

Tvättider
---------
<strong>Måndag till fredag:</strong>
<table width="100%" border="0" cellpadding="3">
<tr class="rubrik2">
<td>Tvättstuga</td>
<td>Torkrum</td>
<td>Mangelrum</td>
</tr>
<tr class="brodtext">
<td>07.00 - 10.00</td>
<td>08.00 - 11.00</td>
<td>10.00 - 13.00</td>
</tr>
<tr class="brodtext">
<td>10.00 - 13.00</td>
<td>11.00 - 14.00</td>
<td>13.00 - 16.00</td>
</tr>
<tr class="brodtext">
<td>13.00 - 16.00</td>
<td>14.00 - 17.00</td>
<td>16.00 - 19.00</td>
</tr>
<tr class="brodtext">
<td>16.00 - 19.00</td>
<td>17.00 - 20.00</td>
<td>19.00 - 21.00</td>
</tr>
<tr class="brodtext">
<td valign="top">19.00 - 21.00</td>
<td valign="top">20.00 - 22.00</td>
<td>07.00 - 10.00, följande dag</td>
</tr>
</table>

<strong>Lördag och söndag</strong> är tvättstugan öppen
mellan 10.00 - 22.00, enligt samma schema som ovan. Övriga tider
utanför schemat är strömmen avstängd till maskinerna för
att grannarna skall få lugn och ro.

Om en bokad tvättid ej påbörjats inom 30 minuter efter bokad tid
kan tiden användas av någon annan.

All tvätt sker på egen risk.

Några trivselregler
===================

Efter avslutad tvätt
--------------------
När du tvättat klart skall du ta bort ludd ur tumlarfiltren,
torka av maskinerna, torka golv i tvätt-, tork- och mangelrum.
Tomma kartonger återvinns i kommunens miljöstation, och sopor och
annat lämnas i vårt sophus.

Vid fel på maskiner
-------------------
Felanmälan görs till Styrelsen.
<a href="mailto:styrelsen@vimpeln16.se">styrelsen@vimpeln16.se</a>

![](bilder/tvatt.gif)
