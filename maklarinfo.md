Fakta / mäklarinformation för Brf Vimpeln16
===========================================

Hemsida: http://www.vimpeln16.se stadgar, årsredovisning, energideklaration m.m.

E-post: styrelsen@vimpeln16.se

Post: Klippgatan 16B, 17147 Solna

Föreningen
----------
Föreningen är en äkta förening, fastigheten köptes 2001

100 st lägenheter, varav 96 brf och 4 hr samt 2 affärslokaler

Total yta för bostads- och hyresrätter är 6 919 𝑚²

Total yta för affärslokaler företag är 160 𝑚²

Föreningen äger marken. Fastighetsbeteckning: Vimpeln 12

Antal portar: Klippgatan 16A, 16B, 16C

Juridisk person godkänns inte som ägare

Ansökan om medlemskap: påskrivet kontrakt skickas till Princip Redovisning

Delat ägande är ok, minsta ägarandel är 10%

Överlåtelseavgift, pantsättningsavgift mm, se vår ekonomiska förvaltning

Vid överlåtelse kommer delar av styrelsen att träffa köparen, innan vi godkänner medlemskapet.

Ekonomisk förvaltning: Alla kontraktsärenden hanteras av

Princip Redovisning, Valhallavägen 165, 115 53 Stockholm

Tel 070-739 56 50, 0703- 39 99 09

Teknisk förvaltning: Driftia förvaltning, tel 08-744 44 33

På gång: Inget för närvarande (aug 2024).

Fastigheten
-----------
Byggår: 1964/65, inflyttning 1965

Ombyggnad: 1993, fasad och balkonger

Renovering:

* 2008/2009 byttes all el: jordfelsbrytare. Tillval: en del lägenheter har trefas
* 2008/2009 stambyte utfört samt nya badrum
* 2010 hissar till EU-standard
* 2012 trapphus och entré
* 2014 miljöhus
* 2017 garage, ramp till parkering och trappa
* 2021 tvättstugor, byte av maskiner

Värme: Fjärrvärme

Köksfläkt: Centralfläkt, se mer på hemsidan / Föreningsinfo

Parkering: 24 platser utomhus och 28 garageplatser. Ansökan till styrelsen.
För närvarande är 5 utomhusplatser och 1 garageplats lediga (aug 2024).

Övrigt
------
Bredband: leverantör Ownit, obligatorisk anslutning och avgift, f.n. 160kr/månad

Kabel-TV: basutbud via Telenor ingår i avgiften

Gemensamt elavtal: enskild förbrukning per lägenhet tillkommer

Lånerum/förråd m.m.:
* Övernattningsrum
* Verkstadsrum
* Mötesrum för 10 personer
* Två tvättstugor, två torkrum
* Cykelrum och barnvagnsrumKällare eller vindsförråd
* Grillplats i trädgården
