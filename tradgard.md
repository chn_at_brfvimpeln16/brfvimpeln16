Vår gröna, granna trädgård
==========================

Vår trädgård
------------
Trädgården består av en stor yta på baksidan
av huset och några
mindre ytor på framsidan och mot parkleken. Alla gång-
och parkytor renoverades 1996.

Grillplats
----------
På gården finns en grillplats och utemöbler, som kan användas
av alla boende. Det är  trevligt att grilla tillsammans, för att göra
nya bekantskaper i huset. För att underlätta för den som grillar efter dig är
det bra om du tar bort askan när den svalnat. En askhink finns under grillen.

Trädgårdsgruppen
----------------
I föreningen finns en livaktig grupp av trädgårdsentusiaster som
planerar, planterar, beskär och klipper gräs. Vi samlas några gånger
per år för planering och träffas då och då på gården för gemensamma
aktiviteter. När andan faller på och det kliar i de gröna fingrarna
ger vi oss ut, både med eller utan sällskap.

Vill du också göra fint i vår trädgård?
---------------------------------------
Kontakta någon i gruppen. Ju fler händer, desto bättre resultat.

* Rajenda Gudibandi, 16 B
* Victor Ramos, 16 A
* Michael Örekärr, 16 C
* Christoffer van Woensel, 16 C

Fixardagar
----------
Fixardagar infaller normalt på
- sista onsdagen innan valborg/1:a maj
- sista söndagen i oktober

Då hjälps vi åt att göra fint i vårt hus och trädgård.
Vi avslutar med en lättare förtäring utomhus om vädret tillåter.

![](bilder/tradgard.gif)
